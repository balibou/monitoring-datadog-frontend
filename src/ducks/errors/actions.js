import * as types from './types';

const error = error => {
  return {
    type: types.ERROR,
    payload: error
  };
};

const clearError = error => {
  return {
    type: types.CLEAR_ERROR,
    payload: error
  };
};

const listError = error => {
  return {
    type: types.LIST_ERROR,
    payload: error
  };
};

export { error, clearError, listError };
