import { combineReducers } from 'redux';
import * as types from './types';

const errorReducer = (
  state = {
    status: false,
    error: ''
  },
  action
) => {
  switch (action.type) {
    case types.ERROR:
      return {
        status: true,
        error: action.payload
      };
    case types.CLEAR_ERROR:
      return {
        status: false,
        error: action.payload
      };
    default:
      return state;
  }
};

const listErrorReducer = (state = [], action) => {
  switch (action.type) {
    case types.LIST_ERROR:
      return [...state, action.payload];
    default:
      return state;
  }
};

const reducer = combineReducers({
  error: errorReducer,
  listError: listErrorReducer
});

export default reducer;
