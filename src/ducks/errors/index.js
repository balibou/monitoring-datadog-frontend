import reducer from './reducer';
import * as errorsActions from './actions';

export { errorsActions };
export default reducer;
