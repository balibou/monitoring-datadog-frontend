const ERROR = 'monitoring-datadog-frontend/error/ERROR';
const CLEAR_ERROR = 'monitoring-datadog-frontend/error/CLEAR_ERROR';
const LIST_ERROR = 'monitoring-datadog-frontend/error/LIST_ERROR';

export { ERROR, CLEAR_ERROR, LIST_ERROR };
