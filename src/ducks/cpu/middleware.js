import axios from 'axios';
import moment from 'moment';
import * as types from './types';
import { successCPU, averageCPU } from './actions';
import { error, clearError, listError } from '../errors/actions';
import {
  getArrayAverage,
  getLast12ArrayItems,
  turnNumberWithTwoDecimals
} from './utils';

const fetchCPULoadAverage = async () => {
  try {
    const { data } = await axios.get(
      'http://localhost:3001/api/cpuLoadAverage'
    );
    return data;
  } catch (error) {
    throw error.response.data;
  }
};

const cpuMiddleware = store => next => async action => {
  if (action.type === types.REQUEST_CPU) {
    const nextAction = next(action);
    const { cpuLoadAverage } = await fetchCPULoadAverage();

    store.dispatch(successCPU(turnNumberWithTwoDecimals(cpuLoadAverage)));
    return nextAction;
  }

  if (action.type === types.SUCCESS_CPU) {
    const nextAction = next(action);

    const { cpu } = store.getState().cpu;
    const cpuPerformanceList = cpu.map(e => e.cpu);

    store.dispatch(
      averageCPU(
        turnNumberWithTwoDecimals(
          getArrayAverage(getLast12ArrayItems(cpuPerformanceList))
        )
      )
    );

    return nextAction;
  }

  if (action.type === types.AVERAGE_CPU) {
    const nextAction = next(action);
    const { status } = store.getState().errors.error;

    const { cpuAvg } = store.getState().cpu;
    const getErrorMessage = word =>
      `High load ${word} an alert - load = ${cpuAvg}, triggered at ${moment().format(
        'LTS'
      )}`;

    if (cpuAvg > 1 && !status) {
      store.dispatch(error(getErrorMessage('generated')));
      store.dispatch(listError(getErrorMessage('generated')));
    }
    if (cpuAvg <= 1 && status) {
      store.dispatch(clearError(getErrorMessage('resolved')));
      store.dispatch(listError(getErrorMessage('resolved')));
    }

    return nextAction;
  }

  return next(action);
};

export default cpuMiddleware;
