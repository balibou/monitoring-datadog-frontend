import reducer from './reducer';
import * as cpuActions from './actions';

export { cpuActions };
export default reducer;
