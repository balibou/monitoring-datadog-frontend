import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import { averageCPU } from './actions';
import { AVERAGE_CPU } from './types';
import { ERROR, CLEAR_ERROR, LIST_ERROR } from '../errors/types';
import cpuMiddleware from './middleware';

const middlewares = [thunk, cpuMiddleware];
const mockStore = configureMockStore(middlewares);

describe('cpu middleware', () => {
  it('triggers nothing when average cpu is lower or equal to 1 and error status is false', () => {
    const state = {
      errors: { error: { status: false } },
      cpu: { cpuAvg: 1 }
    };
    const store = mockStore(() => state);
    store.dispatch(averageCPU(1));
    const actions = store.getActions();

    expect(actions).toHaveLength(1);
    expect(actions[0].type).toEqual(AVERAGE_CPU);
  });

  it('triggers error and listError actions when average cpu is upper than 1 and error status is false', () => {
    const state = {
      errors: { error: { status: false } },
      cpu: { cpuAvg: 1.01 }
    };
    const store = mockStore(() => state);
    store.dispatch(averageCPU(1.01));
    const actions = store.getActions();

    expect(actions).toHaveLength(3);
    expect(actions[0].type).toEqual(AVERAGE_CPU);
    expect(actions[1].type).toEqual(ERROR);
    expect(actions[2].type).toEqual(LIST_ERROR);
  });

  it('triggers nothing actions when average cpu is upper than 1 and error status is true', () => {
    const state = {
      errors: { error: { status: true } },
      cpu: { cpuAvg: 1.01 }
    };
    const store = mockStore(() => state);
    store.dispatch(averageCPU(1.01));
    const actions = store.getActions();

    expect(actions).toHaveLength(1);
    expect(actions[0].type).toEqual(AVERAGE_CPU);
  });

  it('triggers clearError and listError actions when average cpu is lower or equal to 1 and error status is true', () => {
    const state = {
      errors: { error: { status: true } },
      cpu: { cpuAvg: 1 }
    };
    const store = mockStore(() => state);
    store.dispatch(averageCPU(1));
    const actions = store.getActions();

    expect(actions).toHaveLength(3);
    expect(actions[0].type).toEqual(AVERAGE_CPU);
    expect(actions[1].type).toEqual(CLEAR_ERROR);
    expect(actions[2].type).toEqual(LIST_ERROR);
  });
});
