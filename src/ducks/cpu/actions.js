import * as types from './types';

const requestCPU = () => ({
  type: types.REQUEST_CPU
});

const successCPU = cpu => {
  return {
    type: types.SUCCESS_CPU,
    payload: cpu
  };
};

const averageCPU = cpu => {
  return {
    type: types.AVERAGE_CPU,
    payload: cpu
  };
};

export { requestCPU, successCPU, averageCPU };
