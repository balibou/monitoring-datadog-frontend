function getArrayAverage(arr) {
  const sum = arr.reduce((previous, current) => (current += previous));
  return sum / arr.length;
}

function getLast12ArrayItems(arr) {
  return arr.slice(Math.max(arr.length - 12));
}

function turnNumberWithTwoDecimals(num) {
  return Number(num.toFixed(2));
}

export { getArrayAverage, getLast12ArrayItems, turnNumberWithTwoDecimals };
