import { combineReducers } from 'redux';
import moment from 'moment';
import * as types from './types';

const cpuReducer = (state = [], action) => {
  switch (action.type) {
    case types.SUCCESS_CPU:
      return [...state, { cpu: action.payload, date: moment().format('LTS') }];
    default:
      return state;
  }
};

const cpuAvgReducer = (state = 0, action) => {
  switch (action.type) {
    case types.AVERAGE_CPU:
      return action.payload;
    default:
      return state;
  }
};

const reducer = combineReducers({
  cpu: cpuReducer,
  cpuAvg: cpuAvgReducer
});

export default reducer;
