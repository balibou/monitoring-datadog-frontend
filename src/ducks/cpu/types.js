const REQUEST_CPU = 'monitoring-datadog-frontend/cpu/REQUEST_CPU';
const SUCCESS_CPU = 'monitoring-datadog-frontend/cpu/SUCCESS_CPU';
const AVERAGE_CPU = 'monitoring-datadog-frontend/cpu/AVERAGE_CPU';

export { REQUEST_CPU, SUCCESS_CPU, AVERAGE_CPU };
