import React from 'react';
import { Segment, Grid } from 'semantic-ui-react';
import Graph from '../Graph';
import Notification from '../Notification';
import CPUStatistics from '../CPUStatistics';
import ErrorsList from '../ErrorsList';
import styles from './App.module.css';

function App() {
  return (
    <div className={styles.App}>
      <Notification />
      <Grid>
        <Grid.Row>
          <Grid.Column mobile={16} tablet={16} computer={10}>
            <Segment className={styles.chartContainer}>
              <Graph />
            </Segment>
          </Grid.Column>
          <Grid.Column mobile={16} tablet={16} computer={6}>
            <Segment textAlign="center">
              <CPUStatistics />
            </Segment>
            <ErrorsList />
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </div>
  );
}

export default React.memo(App);
