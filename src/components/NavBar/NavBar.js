import React from 'react';
import { Menu, Container, Image } from 'semantic-ui-react';
import datadogLogo from '../../assets/icons/datadogLogo.png';

function NavBar() {
  return (
    <Menu fixed="top">
      <Container>
        <Menu.Item as="a" header>
          <Image size="mini" src={datadogLogo} />
        </Menu.Item>
      </Container>
    </Menu>
  );
}

export default React.memo(NavBar);
