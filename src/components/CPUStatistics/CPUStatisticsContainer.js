import { connect } from 'react-redux';
import CPUStatistics from './CPUStatistics';

const mapStateToProps = ({ cpu }) => ({ cpuAvg: cpu.cpuAvg });

export default connect(mapStateToProps)(CPUStatistics);
