import React from 'react';
import { Statistic, Icon } from 'semantic-ui-react';
import PropTypes from 'prop-types';

function CPUStatistics({ cpuAvg }) {
  return (
    <Statistic>
      <Statistic.Value>
        <Icon name="microchip" />
        {cpuAvg}
      </Statistic.Value>
      <Statistic.Label>CPU average (last 2 minutes)</Statistic.Label>
    </Statistic>
  );
}

CPUStatistics.propTypes = {
  cpuAvg: PropTypes.number
};

export default React.memo(CPUStatistics);
