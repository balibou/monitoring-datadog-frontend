import React from 'react';
import { Segment, List } from 'semantic-ui-react';
import PropTypes from 'prop-types';

function ErrorsList({ listErrors }) {
  if (listErrors.length > 0) {
    return (
      <Segment>
        <List divided relaxed>
          {listErrors.map((error, index) => {
            return (
              <List.Item key={index}>
                <List.Icon
                  name="exclamation triangle"
                  size="large"
                  verticalAlign="middle"
                />
                <List.Content>
                  <List.Description as="a">{error}</List.Description>
                </List.Content>
              </List.Item>
            );
          })}
        </List>
      </Segment>
    );
  }
}

ErrorsList.propTypes = {
  listErrors: PropTypes.array
};

export default React.memo(ErrorsList);
