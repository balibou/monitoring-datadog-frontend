import { connect } from 'react-redux';
import ErrorsList from './ErrorsList';

const mapStateToProps = ({ errors }) => ({ listErrors: errors.listError });

export default connect(mapStateToProps)(ErrorsList);
