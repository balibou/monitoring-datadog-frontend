import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Graph from './Graph';
import { cpuActions } from '../../ducks/cpu';

const mapStateToProps = ({ cpu }) => ({ cpu: cpu.cpu });

const mapDispatchToProps = dispatch =>
  bindActionCreators({ requestCPU: cpuActions.requestCPU }, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Graph);
