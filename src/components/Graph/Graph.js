import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import {
  ResponsiveContainer,
  LineChart,
  Line,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend
} from 'recharts';

class Graph extends PureComponent {
  componentDidMount() {
    this.props.requestCPU();
    this.interval = setInterval(() => this.props.requestCPU(), 10000);
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  render() {
    return (
      <ResponsiveContainer>
        <LineChart
          data={this.props.cpu}
          margin={{ top: 5, right: 30, left: 20, bottom: 5 }}
        >
          <XAxis dataKey="date" />
          <YAxis />
          <CartesianGrid strokeDasharray="3 3" />
          <Tooltip />
          <Legend />
          <Line type="monotone" dataKey="cpu" stroke="#82ca9d" />
        </LineChart>
      </ResponsiveContainer>
    );
  }
}

Graph.propTypes = {
  cpu: PropTypes.array
};

Graph.defaultProps = {
  cpu: []
};

export default Graph;
