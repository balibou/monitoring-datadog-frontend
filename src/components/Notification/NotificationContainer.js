import { connect } from 'react-redux';
import Notification from './Notification';

const mapStateToProps = ({ errors }) => ({ error: errors.error });

export default connect(mapStateToProps)(Notification);
