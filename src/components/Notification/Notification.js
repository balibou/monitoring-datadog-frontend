import React from 'react';
import { SemanticToastContainer, toast } from 'react-semantic-toasts';
import PropTypes from 'prop-types';

function getNotification(error) {
  if (error.error) {
    setTimeout(() => {
      toast({
        type: 'warning',
        icon: 'exclamation triangle',
        title: 'Warning',
        time: 5000,
        description: <p>{error.error}</p>
      });
    }, 1000);
    return <SemanticToastContainer position="top-right" />;
  }
}

function Notification({ error }) {
  return getNotification(error);
}

Notification.propTypes = {
  error: PropTypes.object
};

export default React.memo(Notification);
